﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SimilarityMeasure
{ 
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("SIMILARITYMEASURE.EXE [Source File Name] [Corpus Directory] [Corpus File Mask]");
                Console.WriteLine(@"SIMILARITYMEASURE.EXE c:\myfiles\DHS_RFP.txt c:\BDFiles *.txt");
                Environment.Exit(0);
            }
            if (!File.Exists(args[0]))
            {
                Console.WriteLine($"File {args[0]} does not exist. Exiting.");
                Environment.Exit(0);
            }
            if (!Directory.Exists(args[1]))
            {
                Console.WriteLine($"Folder {args[1]} does not exist. Exiting.");
                Environment.Exit(0);
            }
            
            int threshold = 3; // the token must appear at least three times to be included in the vocabulary
            string filePrefix = @"file://";
            string[] compareFiles = Directory.GetFiles(args[1], args[2]);
            string sourceFile = filePrefix + args[0];

            var scoreList = new List<DocumentScore>();

            try
            {
                // Create instance of calculator
                SimilarityCalculator sc = new SimilarityCalculator();
                //sc.Compare(url1, url2, vocabularyThreshold: threshold); // this program can also compare URLs, but that's not our use case
                string myCompareFile;
                Console.WriteLine($"Comparing {compareFiles.Length} files...");
                int n = 0;
                double similarityScore;

                foreach (string compareFile in compareFiles)
                {
                    myCompareFile = filePrefix + compareFile;
                    n++;
                    if (myCompareFile != sourceFile)
                    {
                        Console.WriteLine("");
                        Console.WriteLine($"Pass {n} of {compareFiles.Length}: Comparing {sourceFile} to {myCompareFile}...");
                        similarityScore = sc.Compare(sourceFile, myCompareFile, vocabularyThreshold: threshold);
                        scoreList.Add(new DocumentScore { fileName = myCompareFile, similarityScore = similarityScore });
                        Console.WriteLine($"The source document is {Math.Round(similarityScore * 100, 0)} % similar.");
                        
                    }
                    else
                    {
                        Console.WriteLine($"Not comparing file {myCompareFile}.");
                    }
                }
                // Write final statistics - "substantially similar" means that there is at least a 70% cosine similarity score
                var substantiallySimilarList = scoreList.Where(o => (o.similarityScore > 0.7)).ToList();
                double pctSimilar = Math.Round(((double)substantiallySimilarList.Count / (double)compareFiles.Length) * (double)100,0);
                Console.WriteLine("");
                Console.WriteLine($"{pctSimilar.ToString()} percent of the corpus files were *substantially* similar to the source file ({sourceFile}).");
                if (substantiallySimilarList.Count > 0) 
                { 
                    Console.WriteLine("The following files were substantially similar:");
                    foreach (DocumentScore d in substantiallySimilarList)
                    {
                        Console.WriteLine($"{Math.Round(d.similarityScore*100,0)}% {d.fileName}");
                    }
                }
                Console.WriteLine("Press any key...");
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
