@echo off

cls
echo ================
echo Example 1: Press any key to compare an email attachment to each file in the BD Library.
echo ================
pause
"C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\SimilarityMeasure\SimilarityMeasure\bin\Release\SimilarityMeasure.exe" "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails\TextFileVersions\8_31_2023_9_19_22_AM_FW_ Reminder_ EPA - FarmTrak Survey Support - MRAS.TXT" "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository\TextFileVersions" *.txt

cls
echo ================
echo Example 2:Press any key to compare a file from the BD Library to each file in the BD Library.
echo ================
pause
"C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\SimilarityMeasure\SimilarityMeasure\bin\Release\SimilarityMeasure.exe" "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository\TextFileVersions\Transition_to_Agile.txt" "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository\TextFileVersions" *.txt

cls
echo ================
echo Example 3: Press any key to compare an email attachment to the entire corpus in the BD Library
echo ================
pause
"C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\SimilarityMeasure\SimilarityMeasure\bin\Release\SimilarityMeasure.exe" "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails\TextFileVersions\9_5_2023_11_12_36_AM_FW_ RFP - JC2PPS - 622369910_JC2PPS - RFP - 622369910 _ FINAL.txt" "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository\TextFileVersions" mergedfiles.txt


