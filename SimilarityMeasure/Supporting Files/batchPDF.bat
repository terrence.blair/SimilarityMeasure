@echo on
rem +++++++++++++++++++
rem TNB 9/7/23 
rem This program converts ALL .MSG email files into their component subject/body and attachments.
rem It then converts ALL PDF, Powerpoint and Word files located in the BDRepository and Emails folders 
rem into text, saving them to folders named TextFileVersions.
rem Requires 2pdf.exe to be default installed on executing computer. Available here -> https://www.cmd2pdf.com/
rem Using the trial version of 2pdf.exe which requires manual confirmation to run
rem Program converts all Powerpoint and Word files into PDFs, and then converts all PDFs into text files
rem We CANNOT convert files online - all files have proprietary information!
rem +++++++++++++++++++

rem ========== Housekeeping - delete unneeded text files - all will be replaced ========== 
c:
erase "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository\TextFileVersions\*.*" /q
pause
erase "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails\TextFileVersions\*.*" /q
pause
cd "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository"

rem ========== 1) Convert BDRepository PPT and PPTX files to PDF files using the 2pdf.exe utility ========== 
"C:\Program Files (x86)\2PDF\2pdf.exe" -src "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository\*.ppt" -dst "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository" -options overwrite:yes

rem ========== 2b) Convert BDRepository DOC and DOCX files to PDF files using the 2pdf.exe utility ========== 
"C:\Program Files (x86)\2PDF\2pdf.exe" -src "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository\*.doc" -dst "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository" -options overwrite:yes

rem ========== 3) Converts all BDRepository PDF files to TXT files using the pdftotext.exe utility ========== 
for /r %%a in (*.pdf) do ..\utilities\pdftotext -enc UTF-8 -layout "%%a" 

rem ========== 4) move all BDRepository .TXT files to final text files folder ========== 
move *.txt "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\BDRepository\TextFileVersions\"

cd "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails"
"C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\ProcessEmailFile\Process_MSG_File\bin\Release\net7.0\Process_MSG_File.exe" "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails\*.MSG" "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails"

rem ========== 5) Convert Emails PPT and PPTX files to PDF files using the 2pdf.exe utility ========== 
"C:\Program Files (x86)\2PDF\2pdf.exe" -src "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails\*.ppt" -dst "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails" -options overwrite:yes

rem ========== 6) Convert Emails DOC and DOCX files to PDF files using the 2pdf.exe utility ========== 
"C:\Program Files (x86)\2PDF\2pdf.exe" -src "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails\*.doc" -dst "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails" -options overwrite:yes

rem ========== 7) Converts all Emails PDF files to TXT files using the pdftotext.exe utility ========== 
for /r %%a in (*.pdf) do ..\utilities\pdftotext -layout "%%a" 

rem ========== 8) move all Emails .TXT files to final text files folder ========== 
move *.txt "C:\Users\terrence.blair\OneDrive - Tetra Tech, Inc\DEsktopBackup\LLMLabsProject\Emails\TextFileVersions\"

